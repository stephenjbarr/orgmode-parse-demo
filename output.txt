"------------------------------"
"FNAME: simple.org"
Right
Document
  { documentText = "\n\n"
  , documentHeadlines =
      [ Headline
          { depth = Depth 1
          , stateKeyword = Nothing
          , priority = Nothing
          , title = "Hello"
          , stats = Nothing
          , tags = []
          , section =
              Section
                { sectionPlannings = Plns (fromList [])
                , sectionClocks = []
                , sectionProperties =
                    fromList [ ( "ID" , "3B429AFB-D8E6-4321-8674-3335F2EF803C" ) ]
                , sectionParagraph = "Hi there\n\n\n"
                }
          , subHeadlines = []
          }
      , Headline
          { depth = Depth 1
          , stateKeyword = Nothing
          , priority = Nothing
          , title = "World"
          , stats = Nothing
          , tags = []
          , section =
              Section
                { sectionPlannings = Plns (fromList [])
                , sectionClocks = []
                , sectionProperties =
                    fromList [ ( "ID" , "A142E345-769D-4F83-B724-B7B5B46C1008" ) ]
                , sectionParagraph = "around the world by Daft Punk\n"
                }
          , subHeadlines = []
          }
      ]
  }
"------------------------------"
"FNAME: test1.org"
Right
Document
  { documentText =
      "#+STARTUP: overview\n#+STARTUP: lognotestate\n#+SEQ_TODO: TODO(t) STARTED(s) WAITING(w) DELEGATED(g) APPT(a) | DONE(d) DEFERRED(f) CANCELLED(c)\n\n\n"
  , documentHeadlines =
      [ Headline
          { depth = Depth 1
          , stateKeyword = Nothing
          , priority = Nothing
          , title = "Hello"
          , stats = Nothing
          , tags = []
          , section =
              Section
                { sectionPlannings = Plns (fromList [])
                , sectionClocks = []
                , sectionProperties =
                    fromList [ ( "ID" , "191B44B4-3770-474F-A5C0-921292359051" ) ]
                , sectionParagraph = "This is section 1.\n\n"
                }
          , subHeadlines = []
          }
      , Headline
          { depth = Depth 1
          , stateKeyword = Nothing
          , priority = Nothing
          , title = "Hello there"
          , stats = Nothing
          , tags = []
          , section =
              Section
                { sectionPlannings = Plns (fromList [])
                , sectionClocks = []
                , sectionProperties =
                    fromList
                      [ ( "CREATED" , "[2017-01-25 Wed 10:18]" )
                      , ( "ID" , "E7016D00-1499-4F43-9644-AD7990240700" )
                      ]
                , sectionParagraph = "Another level 1 section.\n\n\n"
                }
          , subHeadlines = []
          }
      , Headline
          { depth = Depth 1
          , stateKeyword = Nothing
          , priority = Nothing
          , title = "My tasks"
          , stats = Nothing
          , tags = []
          , section =
              Section
                { sectionPlannings = Plns (fromList [])
                , sectionClocks = []
                , sectionProperties =
                    fromList
                      [ ( "CREATED" , "[2017-01-25 Wed 10:18]" )
                      , ( "ID" , "4AFEF99C-D8E7-4CDF-9F30-EA9CEDDB9313" )
                      ]
                , sectionParagraph = ""
                }
          , subHeadlines =
              [ Headline
                  { depth = Depth 2
                  , stateKeyword = Just StateKeyword { unStateKeyword = "TODO" }
                  , priority = Nothing
                  , title = "Something I need to do"
                  , stats = Nothing
                  , tags = []
                  , section =
                      Section
                        { sectionPlannings = Plns (fromList [])
                        , sectionClocks = []
                        , sectionProperties =
                            fromList
                              [ ( "CREATED" , "[2017-01-25 Wed 10:18]" )
                              , ( "ID" , "72FBC724-0987-43A1-8FD0-B149CB731683" )
                              ]
                        , sectionParagraph = ""
                        }
                  , subHeadlines = []
                  }
              , Headline
                  { depth = Depth 2
                  , stateKeyword = Just StateKeyword { unStateKeyword = "DONE" }
                  , priority = Nothing
                  , title = "Something I did"
                  , stats = Nothing
                  , tags = []
                  , section =
                      Section
                        { sectionPlannings = Plns (fromList [])
                        , sectionClocks = []
                        , sectionProperties =
                            fromList [ ( "CREATED" , "[2017-01-25 Wed 10:18]" ) ]
                        , sectionParagraph = ""
                        }
                  , subHeadlines = []
                  }
              ]
          }
      ]
  }
"------------------------------"
"FNAME: timestamped.org"
Right
Document
  { documentText =
      "#+STARTUP: overview\n#+STARTUP: lognotestate\n#+SEQ_TODO: TODO(t) STARTED(s) WAITING(w) DELEGATED(g) APPT(a) | DONE(d) DEFERRED(f) CANCELLED(c)\n\n"
  , documentHeadlines =
      [ Headline
          { depth = Depth 1
          , stateKeyword = Nothing
          , priority = Nothing
          , title = "A task<2017-01-25 Wed>"
          , stats = Nothing
          , tags = []
          , section =
              Section
                { sectionPlannings = Plns (fromList [])
                , sectionClocks = []
                , sectionProperties =
                    fromList
                      [ ( "CREATED" , "[2017-01-25 Wed 16:04]" )
                      , ( "ID" , "B1B319C9-E881-4A95-ACF7-165E1A513EA5" )
                      ]
                , sectionParagraph = ""
                }
          , subHeadlines = []
          }
      , Headline
          { depth = Depth 1
          , stateKeyword = Nothing
          , priority = Nothing
          , title = "A scheduled task"
          , stats = Nothing
          , tags = []
          , section =
              Section
                { sectionPlannings =
                    Plns
                      (fromList
                         [ ( SCHEDULED
                           , Timestamp
                               { tsTime =
                                   DateTime
                                     { yearMonthDay =
                                         YMD'
                                           YearMonthDay
                                             { ymdYear = 2017 , ymdMonth = 1 , ymdDay = 27 }
                                     , dayName = Just "Fri"
                                     , hourMinute = Just ( 7 , 0 )
                                     , repeater = Nothing
                                     , delay = Nothing
                                     }
                               , tsActive = Active
                               , tsEndTime = Nothing
                               }
                           )
                         ])
                , sectionClocks = []
                , sectionProperties =
                    fromList
                      [ ( "CREATED" , "[2017-01-25 Wed 16:04]" )
                      , ( "ID" , "7B6410D9-486B-4410-89B2-5310CDE54909" )
                      ]
                , sectionParagraph = ""
                }
          , subHeadlines = []
          }
      , Headline
          { depth = Depth 1
          , stateKeyword = Just StateKeyword { unStateKeyword = "TODO" }
          , priority = Nothing
          , title = "A TODO task with schedule"
          , stats = Nothing
          , tags = []
          , section =
              Section
                { sectionPlannings =
                    Plns
                      (fromList
                         [ ( SCHEDULED
                           , Timestamp
                               { tsTime =
                                   DateTime
                                     { yearMonthDay =
                                         YMD'
                                           YearMonthDay
                                             { ymdYear = 2017 , ymdMonth = 1 , ymdDay = 26 }
                                     , dayName = Just "Thu"
                                     , hourMinute = Just ( 14 , 0 )
                                     , repeater = Nothing
                                     , delay = Nothing
                                     }
                               , tsActive = Active
                               , tsEndTime = Nothing
                               }
                           )
                         ])
                , sectionClocks = []
                , sectionProperties =
                    fromList
                      [ ( "CREATED" , "[2017-01-25 Wed 16:04]" )
                      , ( "ID" , "8AC6EFB1-CA97-44A3-BD5B-D959C961F2B2" )
                      ]
                , sectionParagraph = ""
                }
          , subHeadlines = []
          }
      , Headline
          { depth = Depth 1
          , stateKeyword = Just StateKeyword { unStateKeyword = "DONE" }
          , priority = Nothing
          , title = "A completed task"
          , stats = Nothing
          , tags = []
          , section =
              Section
                { sectionPlannings =
                    Plns
                      (fromList
                         [ ( SCHEDULED
                           , Timestamp
                               { tsTime =
                                   DateTime
                                     { yearMonthDay =
                                         YMD'
                                           YearMonthDay
                                             { ymdYear = 2017 , ymdMonth = 1 , ymdDay = 25 }
                                     , dayName = Just "Wed"
                                     , hourMinute = Just ( 16 , 0 )
                                     , repeater = Nothing
                                     , delay = Nothing
                                     }
                               , tsActive = Active
                               , tsEndTime = Nothing
                               }
                           )
                         , ( CLOSED
                           , Timestamp
                               { tsTime =
                                   DateTime
                                     { yearMonthDay =
                                         YMD'
                                           YearMonthDay
                                             { ymdYear = 2017 , ymdMonth = 1 , ymdDay = 25 }
                                     , dayName = Just "Wed"
                                     , hourMinute = Just ( 16 , 5 )
                                     , repeater = Nothing
                                     , delay = Nothing
                                     }
                               , tsActive = Inactive
                               , tsEndTime = Nothing
                               }
                           )
                         ])
                , sectionClocks = []
                , sectionProperties =
                    fromList [ ( "CREATED" , "[2017-01-25 Wed 16:04]" ) ]
                , sectionParagraph = ""
                }
          , subHeadlines = []
          }
      , Headline
          { depth = Depth 1
          , stateKeyword = Just StateKeyword { unStateKeyword = "DONE" }
          , priority = Nothing
          , title = "A task with a note"
          , stats = Nothing
          , tags = []
          , section =
              Section
                { sectionPlannings =
                    Plns
                      (fromList
                         [ ( CLOSED
                           , Timestamp
                               { tsTime =
                                   DateTime
                                     { yearMonthDay =
                                         YMD'
                                           YearMonthDay
                                             { ymdYear = 2017 , ymdMonth = 1 , ymdDay = 25 }
                                     , dayName = Just "Wed"
                                     , hourMinute = Just ( 16 , 5 )
                                     , repeater = Nothing
                                     , delay = Nothing
                                     }
                               , tsActive = Inactive
                               , tsEndTime = Nothing
                               }
                           )
                         ])
                , sectionClocks = []
                , sectionProperties =
                    fromList
                      [ ( "CREATED" , "[2017-01-25 Wed 16:05]" )
                      , ( "ID" , "529C642C-9A90-4B6D-A1F1-AEF3BEFB3939" )
                      ]
                , sectionParagraph =
                    "- Note taken on [2017-01-25 Wed 16:06] \\\\\n    This is a note\n"
                }
          , subHeadlines = []
          }
      ]
  }
