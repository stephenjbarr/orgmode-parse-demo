#+STARTUP: overview
#+STARTUP: lognotestate
#+SEQ_TODO: TODO(t) STARTED(s) WAITING(w) DELEGATED(g) APPT(a) | DONE(d) DEFERRED(f) CANCELLED(c)


* Hello
  :PROPERTIES:
  :ID:       191B44B4-3770-474F-A5C0-921292359051
  :END:


  This is section 1.

* Hello there
  :PROPERTIES:
  :CREATED:  [2017-01-25 Wed 10:18]
  :ID:       E7016D00-1499-4F43-9644-AD7990240700
  :END:


  Another level 1 section.


* My tasks
  :PROPERTIES:
  :CREATED:  [2017-01-25 Wed 10:18]
  :ID:       4AFEF99C-D8E7-4CDF-9F30-EA9CEDDB9313
  :END:


** TODO Something I need to do
   :PROPERTIES:
   :CREATED:  [2017-01-25 Wed 10:18]
   :ID:       72FBC724-0987-43A1-8FD0-B149CB731683
   :END:

** DONE Something I did
   :PROPERTIES:
   :CREATED:  [2017-01-25 Wed 10:18]
   :END:
