{-# LANGUAGE OverloadedStrings, ScopedTypeVariables #-}


module Main where

-- import Data.ByteString as BS
-- import Data.Attoparsec.ByteString
import Data.Attoparsec.Text
import Data.OrgMode.Parse
import Data.Text


parseAndPrint :: String -> IO ()
parseAndPrint fname = do

  print "------------------------------"
  print $ "FNAME: " ++ fname
  org_str <- readFile fname
  let org_text :: Text = pack org_str
  let myparse = parseDocument["TODO", "STARTED", "WAITING", "DELEGATED", "APPT", "DONE", "DEFERRED", "CANCELLED"]
  let parsed_doc = parseOnly myparse org_text
  print parsed_doc

main :: IO ()
main = do
  let fnames = ["simple.org", "test1.org", "timestamped.org"]

  mapM_ parseAndPrint fnames
